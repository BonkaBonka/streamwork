package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"path"
	"reflect"
	"strings"
	"syscall"
	"time"

	"github.com/go-redis/redis"
	"github.com/naoina/toml"
)

type StreamConfig struct {
	Command      []string
	WorkingDir   string
	InheritEnv   bool
	UppercaseEnv bool
}

type AppState struct {
	RedisAddr      string
	RedisPass      string
	RedisDb        int
	OutputFormat   string
	BlockTime      int
	TidyOutput     bool
	EnvPrefix      string
	ConfigFileName string

	Streams    map[string]StreamConfig
	Subprocess *exec.Cmd
}

var Version string
var appState AppState

func init() {
	flag.StringVar(&appState.RedisAddr, "redis-addr", "localhost:6379", "redis address")
	flag.StringVar(&appState.RedisPass, "redis-pass", "", "redis password")
	flag.IntVar(&appState.RedisDb, "redis-db", -1, "redis database number")
	flag.StringVar(&appState.OutputFormat, "output", "/tmp/job-%s-%s.out", "output filename format")
	flag.IntVar(&appState.BlockTime, "block-time", 5, "seconds to wait for stream data before checking for new config")
	flag.BoolVar(&appState.TidyOutput, "discard", false, "delete output when subcommand return code is zero")
	flag.StringVar(&appState.EnvPrefix, "env-prefix", "ENTRY_", "prefix to add for each entry key")

	flag.Usage = func() {
		_, _ = fmt.Fprint(os.Stderr, "Streamwork ", Version, "\n\n")
		_, _ = fmt.Fprintf(os.Stderr, "Usage: %s [options] <config.toml>\n\n", path.Base(os.Args[0]))
		_, _ = fmt.Fprint(os.Stderr, "The message variables are supplied in the environment of the command.\n\n")
		_, _ = fmt.Fprint(os.Stderr, "Options:\n\n")
		flag.PrintDefaults()
		_, _ = fmt.Fprint(os.Stderr, "\n")
	}
}

func loadConfig(filename string, tomlStruct interface{}, lastStat os.FileInfo) (newStat os.FileInfo, err error) {
	newStat, err = os.Stat(filename)
	if err != nil {
		return nil, err
	}

	if newStat != lastStat {
		lastStat = newStat

		configFile, err := os.Open(filename)
		if err != nil {
			return nil, err
		}

		err = toml.NewDecoder(configFile).Decode(tomlStruct)
		if err != nil {
			return nil, err
		}

		err = configFile.Close()
		if err != nil {
			return nil, err
		}
	}

	return newStat, nil
}

func collapseLastEventIds(lastEventIds map[string]string) []string {
	count := len(lastEventIds)
	result := make([]string, 2*count)
	i := 0

	for name, id := range lastEventIds {
		result[i] = name
		result[i+count] = id
		i++
	}

	return result
}

func runSubprocess(command StreamConfig, outputFilename string, stream string, id string, message redis.XMessage) (success bool, err error) {
	out, err := os.Create(outputFilename)
	if err != nil {
		return false, err
	}

	defer func() {
		_ = out.Close()
	}()

	log.Printf("command: %v", command.Command)
	appState.Subprocess = exec.Command(command.Command[0], command.Command[1:]...)

	appState.Subprocess.Dir = command.WorkingDir

	appState.Subprocess.Stdout = out
	appState.Subprocess.Stderr = out

	env := make([]string, 2+len(message.Values)+len(os.Environ()))
	env[0] = fmt.Sprintf("STREAM_NAME=%s", stream)
	env[1] = fmt.Sprintf("STREAM_ENTRY_ID=%s", id)

	i := 2
	for k, v := range message.Values {
		if command.UppercaseEnv {
			k = strings.ToUpper(k)
		}
		env[i] = fmt.Sprintf("%s%s=%s", appState.EnvPrefix, k, v)
		i++

		appState.Subprocess.Env = env[:2+len(message.Values)]
	}
	if command.InheritEnv {
		for _, v := range os.Environ() {
			env[i] = v
			i++
		}

		appState.Subprocess.Env = env
	}

	err = appState.Subprocess.Run()
	if err != nil {
		return false, err
	}

	return true, nil
}

func main() {

	flag.Parse()
	configFileName := flag.Args()

	if len(configFileName) != 1 {
		flag.Usage()
		os.Exit(1)
	}

	log.Print("Streamwork ", Version, " starting")
	log.Print("Redis server   : ", appState.RedisAddr)
	log.Print("Redis password : ", len(appState.RedisPass) > 0)
	log.Print("Redis database : ", appState.RedisDb)
	log.Print("Block time     : ", appState.BlockTime)
	log.Print("Output format  : ", appState.OutputFormat)
	log.Print("Tidy output    : ", appState.TidyOutput)
	log.Print("Config file    : ", configFileName[0])

	go func() {
		sigchan := make(chan os.Signal, 1)
		signal.Notify(sigchan, os.Interrupt, syscall.SIGTERM)

		tidychan := make(chan os.Signal, 1)
		signal.Notify(tidychan, syscall.SIGUSR1)

		for {
			select {
			case <-sigchan:
				if appState.Subprocess != nil && !appState.Subprocess.ProcessState.Exited() {
					log.Print("killing subprocess")
					if err := appState.Subprocess.Process.Kill(); err != nil {
						log.Fatalf("failed to kill: %v", err)
					}
				}

				log.Print("exiting")
				os.Exit(1)
			case <-tidychan:
				appState.TidyOutput = !appState.TidyOutput
				log.Printf("setting removal of output of successful subcommands to %v", appState.TidyOutput)
			}
		}
	}()

	client := redis.NewClient(&redis.Options{
		Addr:     appState.RedisAddr,
		Password: appState.RedisPass,
		DB:       appState.RedisDb,
	})
	defer func() {
		err := client.Close()
		if err != nil {
			log.Fatalf("redis.Close: %v", err.Error())
		}
	}()

	var lastStat os.FileInfo
	var lastEventIds map[string]string

	for {
		newStat, err := loadConfig(configFileName[0], &appState.Streams, lastStat)
		if err != nil {
			log.Fatalf("Error: loadConfig '%s': %v", configFileName[0], err.Error())
		}

		if !reflect.DeepEqual(newStat, lastStat) {
			lastStat = newStat

			lastEventIds = make(map[string]string)

			for streamName := range appState.Streams {
				lastEventIds[streamName] = "0"
			}
		}

		if len(appState.Streams) < 1 {
			log.Print("no streams configured - sleeping")
			time.Sleep(time.Duration(appState.BlockTime) * time.Second)
			continue
		}

		log.Print("reading streams: ", lastEventIds)
		xr, err := client.XRead(&redis.XReadArgs{
			Block:   time.Duration(appState.BlockTime) * time.Second,
			Count:   1,
			Streams: collapseLastEventIds(lastEventIds),
		}).Result()
		if err != nil {
			// This is totally weird - no data isn't an error but the library treats it as such
			if err != redis.Nil {
				log.Fatalf("Stream Receive Failed: %v", err.Error())
			}
			continue
		}

		for _, streamData := range xr {
			streamConfig := appState.Streams[streamData.Stream]

			for _, msg := range streamData.Messages {
				outputFilename := fmt.Sprintf(appState.OutputFormat, streamData.Stream, msg.ID)
				log.Printf("output filename: %s", outputFilename)

				success, err := runSubprocess(streamConfig, outputFilename, streamData.Stream, msg.ID, msg)
				if err != nil {
					log.Printf("runSubprocess: %v", err.Error())
				}
				if success {
					// TODO: Probably ought to check that exactly one message got deleted
					_, err := client.XDel(streamData.Stream, msg.ID).Result()
					if err != nil {
						log.Fatalf("client.XDel %s: %v", msg.ID, err.Error())
					}
					if appState.TidyOutput {
						log.Print("removing output: ", outputFilename)
						err := os.Remove(outputFilename)
						if err != nil {
							log.Fatalf("os.Remove failed: %v", err.Error())
						}
					}
				}
				lastEventIds[streamData.Stream] = msg.ID
			}
		}
	}
}
